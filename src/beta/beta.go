package main

import "fmt"

func main() {
	fmt.Println("I can do maths!")
	fmt.Println("2 + 2 is ", add(2, 2))
	fmt.Println("9 times 5 is ", times(9, 5))
	fmt.Println("The greatest common divisor of 135 and 420 is ", gcd(135, 420))
}

func add(a, b int) int {
	return a + b
}

func times(a, b int) int {
	return a * b
}

func gcd(a, b int) int {
	// Euclidean algorithm
	for b != 0 {
		a, b = b, a%b
	}
	return a
}
