package main

import "testing"

func TestSayHello(t *testing.T) {
	resp := sayHello("Bob")
	expect := "Hello, Bob! \U0001f609 "
	if resp != expect {
		t.Errorf("Expected: %q\nReceived:%q", expect, resp)
	}
}

func TestSayHelloFrench(t *testing.T) {
	resp := sayHelloFrench("Bob")
	expect := "Bonjour, Bob! \u2764 "
	t.Log(resp, expect)
	// if resp != expect {
	// 	t.Errorf("Expected: %q\nReceived:%q", expect, resp)
	// }
}
