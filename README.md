# Golang Gitlab CI/CD Demo [![pipeline status](https://gitlab.com/flying_kiwi/go-ci-demo/badges/master/pipeline.svg)](https://gitlab.com/flying_kiwi/go-ci-demo/commits/master) [![coverage report](https://gitlab.com/flying_kiwi/go-ci-demo/badges/master/coverage.svg)](https://gitlab.com/flying_kiwi/go-ci-demo/commits/master)

A simple project to demonstrate how to use Gitlab CI/CD with Go.

Install dependencies with:
```
go get -v -d ./...
```

Run the scripts with:
```
go run src/alpha.go
go run src/beta/beta.go
```

Run the tests with
```
go test -v ./...
```
